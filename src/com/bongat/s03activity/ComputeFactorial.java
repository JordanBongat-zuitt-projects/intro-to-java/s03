package com.bongat.s03activity;

import java.util.Scanner;

public class ComputeFactorial {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input a number to compute the factorial");
        try {
            int num = in.nextInt();
            int answer = 1;
            int i = 1;
            if (num < 0) {
                System.out.println("Can't compute factorial of a negative number");
            } else if (num == 0) {
                System.out.println("Input is zero");
            } else {
                while (i <= num) {
                    answer = answer * i;
                    i++;
                }
                System.out.println(answer);
            }
        } catch (Exception err) {
            System.out.println("Input is not a number");
        }
    }
}
